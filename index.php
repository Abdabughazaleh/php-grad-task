<?php
/**
 * Created by Mohamemed Azmi.
 * User: ROOT
 * Date: 1/30/2019
 * Time: 10:13 PM
 */

class StudentsGrads
{
    private $array = array();
    private $rounding = null;
    private $n = 0;

    public function __construct()
    {
        $this->array = [4, 73, 67, 38, 33];
        $this->n = $this->array[0];
        $this->rounding = array();
        self::initFunction();
    }

    function initFunction()
    {

        if ($this->n <= 60 && $this->n > 0) :

            for ($i = 1; $i < count($this->array); $i++):
                if ($this->array[$i] >= 1 && $this->array[$i] <= 100):
                    self::roundAndGrade($this->array[$i]);
                else :
                    echo 'not valid';
                endif;
            endfor;
        else:
            echo 'not valid';
        endif;
    }

    function printArray()
    {
        echo '<table border="1">
    <tr>
        <td style="background-color:#5db7ed ">ID</td>
        <td style="background-color:#5db7ed ">Original Grade</td>
        <td style="background-color:#5db7ed ">Final Grade</td>
    </tr> ';
        foreach ($this->rounding as $key => $value):

            echo '<tr>
                <td style="background-color:#5db7ed ">' . $key . '</td>
                <td style="background-color:#46d2b8 ">' . $this->array[$key + 1] . '</td>
                <td style="background-color:#46d2b8 ">' . $value . '</td>
            </tr>';

        endforeach;
        echo '</table>';
    }

    /**
     * @param $grade ,is a student grad pass by loop .
     */
    function roundAndGrade($grade = 0)
    {
        if ($grade > 37) :
            $mod = $grade % 5;
            if ($mod >= 3) {
                array_push($this->rounding, $grade + (5 - $mod));
            } else {
                array_push($this->rounding, $grade);
            }
        else:
            array_push($this->rounding, $grade);
        endif;
    }
}


$obj = new StudentsGrads();
$obj->printArray();


